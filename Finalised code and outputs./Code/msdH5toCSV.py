"""
This code was edited to take a different set of columns from the file, and to
partially clean the data.

Alexis Greenstreet (October 4, 2015) University of Wisconsin-Madison
This code is designed to convert the HDF5 files of the Million Song Dataset
to a CSV by extracting various song properties.
The script writes to a "SongCSV.csv" in the directory containing this script.

Credit:
This HDF5 to CSV code makes use of the following example code provided
at the Million Song Dataset website
(Home>Tutorial/Iterate Over All Songs,
http://labrosa.ee.columbia.edu/millionsong/pages/iterate-over-all-songs),
Which gives users the following code to get all song titles:
import os
import glob
import hdf5_getters
def get_all_titles(basedir,ext='.h5') :
    titles = []
    for root, dirs, files in os.walk(basedir):
        files = glob.glob(os.path.join(root,'*'+ext))
        for f in files:
            h5 = hdf5_getters.open_h5_file_read(f)
            titles.append( hdf5_getters.get_title(h5) )
            h5.close()
    return titles
"""

import sys
import os
import glob
import hdf5_getters
import re

class Song:
    songCount = 0
    # songDictionary = {}

    def __init__(self, songID):
        self.id = songID
        Song.songCount += 1
        # Song.songDictionary[songID] = self

        self.artistId = None
        self.artistName = None
        self.title = None
        self.year = None
        self.albumName = None
        self.audioHash = None
        self.danceability = None
        self.energy = None
        self.keySignature = None
        self.KeySignatureConfidence = None
        self.loudness = None
        self.mode = None
        self.modeConfidence = None
        self.tempo = None
        self.timeSignature = None
        self.timeSignatureConfidence = None

    def displaySongCount(self):
        print("Total Song Count %i") % Song.songCount

    def displaySong(self):
        print("ID: %s") % self.id


def main():
    outputFile1 = open('SongCSV.csv', 'r+')
    csvRowString = ""

    #################################################
    #if you want to prompt the user for the order of attributes in the csv,
    #leave the prompt boolean set to True
    #else, set 'prompt' to False and set the order of attributes in the 'else'
    #clause
    prompt = False;
    #################################################

    #################################################
    #change the order of the csv file here
    #Default is to list all available attributes (in alphabetical order)
    csvRowString = ("SongID,ArtistID,ArtistName,SongName,YearRelease,AlbumName,HashCode,Danceability,Energy,KeySignature,KeyConfidence,Loudness,Mode,ModeConf,Tempo,TimeSignature,TimeConfidence")
        #################################################

    csvAttributeList = re.split('\W+', csvRowString)
    for i, v in enumerate(csvAttributeList):
        csvAttributeList[i] = csvAttributeList[i].lower()
    outputFile1.write("SongNumber,");
    outputFile1.write(csvRowString + "\n");
    csvRowString = ""

    #################################################


    #Set the basedir here, the root directory from which the search
    #for files stored in a (hierarchical data structure) will originate
    basedir = "MillionSongSubset/data" # "." As the default means the current directory
    ext = ".h5" #Set the extension here. H5 is the extension for HDF5 files.
    #################################################

    #FOR LOOP
    for root, dirs, files in os.walk(basedir):
        files = glob.glob(os.path.join(root,'*'+ext))
        for f in files:
            print(f)

            songH5File = hdf5_getters.open_h5_file_read(f)
            song = Song(str(hdf5_getters.get_song_id(songH5File)))
            song.artistID = str(hdf5_getters.get_artist_id(songH5File))
            song.artistName = str(hdf5_getters.get_artist_name(songH5File))
            song.title = str(hdf5_getters.get_title(songH5File))
            song.year = str(hdf5_getters.get_year(songH5File))
            song.albumName = str(hdf5_getters.get_release(songH5File))
            song.audioHash = str(hdf5_getters.get_audio_md5(songH5File))
            song.danceability = str(hdf5_getters.get_danceability(songH5File))
            song.energy = str(hdf5_getters.get_energy(songH5File))
            song.keySignature = str(hdf5_getters.get_key(songH5File))
            song.keySignatureConfidence = str(hdf5_getters.get_key_confidence(songH5File))
            song.loudness = str(hdf5_getters.get_loudness(songH5File))
            song.mode = str(hdf5_getters.get_mode(songH5File))
            song.modeConfidence = str(hdf5_getters.get_mode_confidence(songH5File))
            song.tempo = str(hdf5_getters.get_tempo(songH5File))
            song.timeSignature = str(hdf5_getters.get_time_signature(songH5File))
            song.timeSignatureConfidence = str(hdf5_getters.get_time_signature_confidence(songH5File))



            #print song count
            csvRowString += str(song.songCount) + ","

            for attribute in csvAttributeList:

                if attribute == 'AlbumName'.lower():
                    albumName = song.albumName
                    albumName = albumName.replace("b'","")
                    albumName = re.sub('[^a-zA-Z0-9 \n\.]', '', albumName)
                    csvRowString += "\"" + albumName + "\""
                elif attribute == 'ArtistID'.lower():
                    artistID = song.artistID
                    artistID = artistID.replace("b'","")
                    artistID = re.sub('[^a-zA-Z0-9 \n\.]', '', artistID)
                    csvRowString += "\"" + artistID + "\""
                elif attribute == 'ArtistName'.lower():
                    artistName = song.artistName
                    artistName = artistName.replace("b'","")
                    artistName = re.sub('[^a-zA-Z0-9 \n\.]', '', artistName)
                    csvRowString += "\"" + artistName + "\""
                elif attribute == 'Danceability'.lower():
                    csvRowString += song.danceability
                elif attribute == 'KeySignature'.lower():
                    csvRowString += song.keySignature
                elif attribute == 'KeyConfidence'.lower():
                    csvRowString += song.keySignatureConfidence
                elif attribute == 'SongID'.lower():
                    songID = song.id
                    songID = songID.replace("b'","")
                    songID = re.sub('[^a-zA-Z0-9 \n\.]', '', songID)
                    csvRowString += "\"" + songID + "\""
                elif attribute == 'Tempo'.lower():
                    csvRowString += song.tempo
                elif attribute == 'TimeSignature'.lower():
                    csvRowString += song.timeSignature
                elif attribute == 'TimeConfidence'.lower():
                    csvRowString += song.timeSignatureConfidence
                elif attribute == 'SongName'.lower():
                    songName = song.title
                    songName = songName.replace("b'","")
                    songName = re.sub('[^a-zA-Z0-9 \n\.]', '', songName)
                    csvRowString += "\"" + songName + "\""
                elif attribute == 'YearRelease'.lower():
                    csvRowString += song.year
                elif attribute == 'HashCode'.lower():
                    audioHash = song.audioHash
                    audioHash = audioHash.replace("b'","")
                    audioHash = re.sub('[^a-zA-Z0-9 \n\.]', '', audioHash)
                    csvRowString += "\"" + audioHash + "\""
                elif attribute == 'Energy'.lower():
                    csvRowString += song.energy
                elif attribute == 'Loudness'.lower():
                    csvRowString += song.loudness
                elif attribute == 'Mode'.lower():
                    csvRowString += song.mode
                elif attribute == 'ModeConf'.lower():
                    csvRowString += song.modeConfidence
                else:
                    csvRowString += "Erm. This didn't work. Error. :( :(\n"

                csvRowString += ","

            #Remove the final comma from each row in the csv
            lastIndex = len(csvRowString)
            csvRowString = csvRowString[0:lastIndex-1]
            csvRowString += "\n"
            outputFile1.write(csvRowString)
            csvRowString = ""

            songH5File.close()

    outputFile1.close()

main()
