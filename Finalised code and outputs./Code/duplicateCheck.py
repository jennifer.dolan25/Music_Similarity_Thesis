import csv

f1 = csv.reader(open('userPlaylistClean.csv', 'r'))
writer = csv.writer(open("userPlaylistClean2.csv", "w"))
#prevUser = ""
#This set is to stop the addtion of row duplicates.
currentSet = set()
x = 0
prevUser = []
user = []
for row in f1:
    if x == 0:
        prevUser = row
        x = 1
    else:
        user = row
    if user != []:
        if prevUser[0] == user[0]:
            if prevUser[0] not in currentSet:
                writer.writerow(prevUser)
            writer.writerow(user)
            currentSet.add(prevUser[0])
            currentSet.add(user[0])
            prevUser = user
        else:
            prevUser = user
