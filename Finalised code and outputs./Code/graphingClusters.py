import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

data = pd.read_csv('SilhouetteOutput200steps.csv')
X = data[['Clusters']]
Y = data[['Silhouette Average']]

plt.ylabel('Silhouette Average')
plt.xlabel('Number of clusters')
my_xticks = range(2, 10000, 200)
plt.xticks(X, my_xticks)
plt.plot(X,Y)
plt.show()

#High point at 52 before steady fall.
