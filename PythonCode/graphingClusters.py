import matplotlib.pyplot as plt
import pandas as pd

data = pd.read_csv('clustersAndSilhouette.csv')
X = data[['Clusters']]
Y = data[['Silhouette Average']]

plt.ylabel('Silhouette Average')
plt.xlabel('Number of clusters')
plt.plot(X,Y)
plt.show()

#High point at 52 before steady fall.
