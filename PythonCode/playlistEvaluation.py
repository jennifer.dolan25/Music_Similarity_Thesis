import csv
import math

#Variables needed
PrevName = ''
SongToClusterDict = {}
ClusterToCountDict = {}
UserToScoreDict = {}
NumberOfClusters = 0
PlaylistSongAmount = 0
TotalScore = 0

#Read both CSVs
ClusterFile = open('10ClusterOutput.csv', 'r')
SongClusters = csv.reader(ClusterFile)
PlaylistFile = open('userPlaylistClean.csv', 'r')
UserPlaylists = csv.reader(PlaylistFile)

#Extract dictionary from clusters info, song_ID:clusters
for line in SongClusters:
    SongID = line[0]
    ClusterIndex = int(line[10])
    ClusterNum = ClusterIndex + 1
    if(ClusterNum > NumberOfClusters):
        NumberOfClusters = ClusterNum
    SongToClusterDict[SongID] = ClusterIndex

#Initialise cluster dictionary:
for i in range(0, NumberOfClusters):
    ClusterToCountDict[i] = 0

for line in UserPlaylists:
    #Finished with this user:
    Name = line[0]
    SongID = line[1]
    if(Name != PrevName and PrevName != ''):
        #Count up clusters for Previous User
        UserToScoreDict[PrevName] = 0
        for i in range(0, NumberOfClusters):
            Score = ClusterToCountDict.get(i, 0)
            #Don't count scores of 1
            if(Score > 1):
                UserToScoreDict[PrevName] += (Score)
            ClusterToCountDict[i] = 0 #Clear value
        PlaylistSongAmount = 0

    #Compare line[1] against the dictionary and add to cluster struct
    ClusterToCountDict[SongToClusterDict[SongID]] += 1
    PlaylistSongAmount += 1
    PrevName = Name

#Finally add scores for the last user
UserToScoreDict[PrevName] = 0
for i in range(0, len(ClusterToCountDict)):
    Score = ClusterToCountDict.get(i, 0)
    #Don't count scores of 1
    if(Score > 1):
        UserToScoreDict[PrevName] += (Score)

for key in UserToScoreDict:
    print(key, UserToScoreDict[key])
    TotalScore += UserToScoreDict[key]

print("Total for 10 K-means clusters:", TotalScore)
PlaylistFile.close()
ClusterFile.close()
