import pandas as pd
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
#Number of predefined clusters
#x = input("Please enter the number of clusters you wish to test: ")
#x = int(x)
x = 52

#Read in data from generated csv file.
data = pd.read_csv('SongCSV.csv')
#Select attributes to cluster on.
test = data[['KeySignature','Tempo','TimeSignature','Loudness']]

silhouetteResults = open("SilhouetteScores2.txt","w")

#Set number of cluster you want to create.
range_n_clusters = [x]#[5000,6000,7000]
#range_n_clusters = range(2, 10000)

for n_clusters in range_n_clusters:
    #Generate clusters with random seed of 10 for consistancy
    clusterer = KMeans(n_clusters=n_clusters, random_state=10)
    cluster_labels = clusterer.fit_predict(test)
    cluster_map = pd.DataFrame()
    #Mapping songNumber labels for later printing.
    cluster_map['Song Number'] = data[['SongNumber']]
    cluster_map['Song Name'] = data[['SongName']]
    cluster_map['Aritist Name'] = data[['ArtistName']]
    cluster_map['Year Release'] = data[['YearRelease']]
    cluster_map['Album Name'] = data[['AlbumName']]
    cluster_map['Key Signature'] = data[['KeySignature']]
    cluster_map['Tempo'] = data[['Tempo']]
    cluster_map['TimeSignature'] = data[['TimeSignature']]
    cluster_map['Loudness'] = data[['Loudness']]
    cluster_map['cluster'] = clusterer.labels_

    #The silhouette_score gives the average value for all the samples.
    #This gives us an overall score of how well the data has fit to
    #x number of clusters.
    silhouette_avg = silhouette_score(test, cluster_labels)
    #textToWrite = ("First number clusters, Second silhouette average: ")
    #silhouetteResults.write(textToWrite + str(n_clusters) + '\t' + str(silhouette_avg) + '\n')

    #print("For n_clusters =", n_clusters,
    #      "The average silhouette_score is :", silhouette_avg)
    #with pd.option_context('display.max_rows', None, 'display.max_columns', None):
        #print(str(cluster_map['SongNumber']) + '\n' +str(cluster_map['cluster'])+'\n'  )



    #Writing cluster information to text file.
    #This is to keep track of what songs have been clustered together
    #This will be the data that is pulled for visualizations.
    clusterWriter = open("52ClusterOutput.csv","w")
    cluster_map.to_csv(clusterWriter)
