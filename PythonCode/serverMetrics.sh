curl -u elastic: -X PUT -H 'Content-Type: application/json'
http://localhost:9200/millionsongsubset_full -d '{
   "settings":{
      "number_of_shards":1,
      "number_of_replicas":0
   },
   "mappings":{
      "metric":{
         "properties":{
            "analysis sample rate":{
               "type":"float"
            },
            "artist 7digitalid":{
               "type":"int"
            },
            "artist familiarity":{
               "type":"float"
            },
            "artist hotttnesss":{
               "type":"float"
            },
            "artist id":{
               "type":"string"
            },
            "artist latitude":{
               "type":"float"
            },
            "artist location":{
               "type":"string"
            },
            "artist longitude":{
               "type":"float"
            },
            "artist mbid":{
               "type":"string"
            },
            "artist mbtags":{
               "type":"array string"
            },
            "artist mbtags count":{
               "type":"array int"
            },
            "artist name":{
               "type":"string"
            },
            "artist playmeid":{
               "type":"int"
            },
            "artist terms":{
               "type":"array string"
            },
            "artist terms freq":{
               "type":"array float"
            },
            "artist terms weight":{
               "type":"array float"
            },
            "audio md5":{
               "type":"string"
            },
            "bars confidence":{
               "type":"array float"
            },
            "bars start":{
               "type":"array float"
            },
            "beats confidence":{
               "type":"array float"
            },
            "beats start":{
               "type":"array float"
            },
            "danceability":{
               "type":"float"
            },
            "duration":{
               "type":"float"
            },
            "end of fade in":{
               "type":"float"
            },
            "energy":{
               "type":"float"
            },
            "key":{
               "type":"int"
            },
            "key confidence":{
               "type":"float"
            },
            "loudness":{
               "type":"float"
            },
            "mode":{
               "type":"int"
            },
            "mode confidence":{
               "type":"float"
            },
            "release":{
               "type":"string"
            },
            "release 7digitalid":{
               "type":"int"
            },
            "sections confidence":{
               "type":"array float"
            },
            "sections start":{
               "type":"array float"
            },
            "segments confidence":{
               "type":"array float"
            },
            "segments loudness max":{
               "type":"array float"
            },
            "segments loudness max time":{
               "type":"array float"
            },
            "segments loudness max start":{
               "type":"array float"
            },
            "segments pitches":{
               "type":"2D array float"
            },
            "segments start":{
               "type":"array float"
            },
            "segments timbre":{
               "type":"2D array float"
            },
            "similar artists":{
               "type":"array string"
            },
            "song hotttnesss":{
               "type":"float"
            },
            "song id":{
               "type":"string"
            },
            "start of fade out":{
               "type":"float"
            },
            "tatums confidence":{
               "type":"array float"
            },
            "tatums start":{
               "type":"array float"
            },
            "tempo":{
               "type":"float"
            },
            "time signature":{
               "type":"int"
            },
            "time signature confidence":{
               "type":"float"
            },
            "title":{
               "type":"string"
            },
            "track id":{
               "type":"string"
            },
            "track 7digitalid":{
               "type":"int"
            },
            "year":{
               "type":"int"
            }
         }
      }
   }
}'
