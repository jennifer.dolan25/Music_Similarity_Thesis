import csv

#Sorting through userplaylist to remove any rows with songs that are not
#in the clustered output.
with open('52ClusterOutput.csv', 'r') as clusteredSongs:
    cluster_indices = dict((r[0], i) for i, r in enumerate(csv.reader(clusteredSongs)))

with open('userPlaylist.csv', 'r') as user:
    with open('userPlaylistCleaningP1.csv', 'w') as output:
        reader = csv.reader(user)
        writer = csv.writer(output)

        for row in reader:
            index = cluster_indices.get(row[1])
            if index is not None:
                writer.writerow(row)

clusteredSongs.close()
user.close()
output.close()

#Sorting through output to make sure that each playlist has at least 2 songs in it.
userPlaylist = csv.reader(open('userPlaylistCleaningP1.csv', 'r'))
writer = csv.writer(open("userPlaylistClean.csv", "w"))
#prevUser = ""
#This set is to stop the addtion of row duplicates.
currentSet = set()
x = 0
prevUser = []
user = []
for row in userPlaylist:
    if x == 0:
        prevUser = row
        x = 1
    else:
        user = row
    if user != []:
        if prevUser[0] == user[0]:
            if prevUser[0] not in currentSet:
                writer.writerow(prevUser)
            writer.writerow(user)
            currentSet.add(prevUser[0])
            currentSet.add(user[0])
            prevUser = user
        else:
            prevUser = user
