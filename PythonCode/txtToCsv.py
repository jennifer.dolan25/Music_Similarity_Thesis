import csv

with open('kaggle_visible_evaluation_triplets.txt') as fin, open('userPlaylist.csv', 'w') as fout:
    for line in fin:
        fout.write(line.replace('\t', ','))
