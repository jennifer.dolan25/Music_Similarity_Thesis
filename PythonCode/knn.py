import csv
import seaborn as sns
import pandas as pd
import numpy as np
import matplotlib

%matplotlib inline
data = pd.read_csv('SongCSV.csv')
#print(data.head(1))

sns.pairplot(data, vars = ['Energy','Danceability','KeySignature','Tempo','TimeSignature','Loudness','Mode'], size=5)
