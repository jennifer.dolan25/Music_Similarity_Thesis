
//Setting size of window
var width = 500,
      height = 500,
      start = -1,
      end = 1,
      margin = {top:50,bottom:50,left:50,right:50};
var songName = [];
      d3.csv("SongCSV.csv",function(csv){
            csv.map(function(d){
                songName.push({ID: d.SongName});
            })
      });
      console.log("songName",songName);
      onFilter();

      d3.select('#filterOn').on('keyup', onFilter);

      function onFilter(){

        var filterText = d3.select('#filterOn').property('value');

        filteredData = songName;
         if (filterText == ""){
            var filteredData = songName.filter(function(d){
              return (d.ID.indexOf(filterText) === 0)
            });
          }

        d3.select('#filteredList').html(
          Object.keys(songName).map(function(d){
            return d.ID;
          }).join("<br/>")
        );
      }
/*
//Manually setting co-ordinates to follow Bass Clef pattern
var nodes = [
    { id: "1", x: (width/2), y: (height/2)},//x: 250, y: 250 },
    { id: "2", x: (width/2)-80, y: (height/2)-35},//x: 195, y: 215 },
    { id: "3", x: (width/2)-85, y: (height/2)-120},//x: 190, y: 160 },
    { id: "4", x: (width/2)+10, y: (height/2)-185},//x: 235, y: 95 },
    { id: "5", x: (width/2)+150, y: (height/2)-150},//x: 355, y: 120 },
    { id: "6", x: (width/2)+200, y: (height/2)-60},//x: 375, y: 150 },
    { id: "7", x: (width/2)+180, y: (height/2)+40},//x: 400, y: 230 },
    { id: "8", x: (width/2)+120, y: (height/2)+120},//x: 370, y: 315 },
    { id: "9", x: (width/2)+40, y: (height/2)+170},//x: 290, y: 385 },
    { id: "10", x: (width/2)-40, y: (height/2)+200},//x: 210, y: 415 }
];

var links = [{ source: 1, target: 2 },
            { source: 2, target: 3 },
            { source: 3, target: 4 },
            { source: 4, target: 5 },
            { source: 5, target: 6 },
            { source: 6, target: 7 },
            { source: 7, target: 8 },
            { source: 8, target: 9 },
            { source: 9, target: 10 },
            { source: 1, target: 2 },
            { source: 10, target: 9 },
            { source: 9, target: 8 },
            { source: 8, target: 7 },
            { source: 7, target: 6 },
            { source: 6, target: 5 },
            { source: 5, target: 4 },
            { source: 4, target: 3 },
            { source: 3, target: 2 },
            { source: 2, target: 1 }
          ];
//------------------------------------------------//
var svg = d3.select('body').append('svg')
    .attr('width', width)
    .attr('height', height);

var link = svg.selectAll('.link')
    .data(links)
    .style("stroke-width", 5)
    .enter().append("path")
    .attr('class', 'link')
    .style("fill", "blue");

var node = svg.selectAll('.node')
    .data(nodes)
    .enter().append('circle')
    .attr('class', 'node');
}

      var simulation = d3.forceSimulation()
        .force("link", d3.forceLink().id(function(d) { return d.id; }).strength(0))
        .on("tick", ticked);

        simulation.nodes(nodes);
        simulation.force("link").links(links);

  function ticked() {
      link.attr("d", function(d) {
        var dx = d.target.x - d.source.x,
        dy = d.target.y - d.source.y,
        dr = Math.sqrt(dx * dx + dy * dy);
        return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 " + d.target.x + "," + d.target.y;
      });
      /*link.attr("d", function(d) {
        var dx = d.target.x - d.source.x,
        dy = d.target.y - d.source.y,
        dr = Math.sqrt(dx * dx + dy * dy);
        return 1 - ("M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 " + d.target.x + "," + d.target.y);
      });//

      node.attr('r', 5.5)
          .attr('cx', function(d) { return d.x; })
          .attr('cy', function(d) { return d.y; });
}
*/


//For reading in data from csv file
  /*  d3.csv("graphTest.csv", function(error, links) {
      if (error) throw error;

      var nodesByName = {};

      // Create nodes for each unique source and target.
      links.forEach(function(link) {
        link.source = nodeByName(link.source);
        link.target = nodeByName(link.target);
      });

      // Extract the array of nodes from the map by name.
      var nodes = d3.values(nodesByName);
*/
